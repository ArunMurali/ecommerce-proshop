import React, { FC } from "react";

import { FaShoppingCart, FaUser, FaBars } from "react-icons/fa";
import logo from "../assets/logo.png";
// import { LinkContainer } from "react-router-bootstrap";
import { NavLink } from "react-router-dom";

interface ShoppingCartHeaderProps {}

const ShoppingCartHeader: FC<ShoppingCartHeaderProps> = ({ cartItemCount }) => {
  return (
    <>
      <nav className="navbar navbar-expand-lg bg-body-tertiary header-nav dark">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">
            <img src={logo} alt="Logo " /> ProShop
          </NavLink>
          <div
            className="navbar-toggler ms-auto bars-icon"
            // type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="true"
            aria-label="Toggle navigation"
          >
            <FaBars />
          </div>

          <div
            className="navbar-collapse collapse me-auto"
            id="navbarTogglerDemo01"
          >
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink
                  className={({ isActive }) =>
                    isActive ? "nav-link active" : "nav-link"
                  }
                  aria-current="page"
                  to="cart"
                >
                  <FaShoppingCart /> Cart
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  className={({ isActive }) =>
                    isActive ? "nav-link active" : "nav-link"
                  }
                  to="/"
                >
                  <FaUser /> Sign In
                </NavLink>
              </li>
            </ul>
            {/* <form className="d-flex" role="search">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button className="btn btn-outline-success" type="submit">
                Search
              </button>
            </form> */}
          </div>
        </div>
      </nav>
    </>
  );
};

export default ShoppingCartHeader;
