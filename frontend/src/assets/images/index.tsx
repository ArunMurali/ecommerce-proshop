export default {
  logo_black: new URL("./main-logo-black-transparent.svg", import.meta.url)
    .href,
  logo_white: new URL("./main-logo-white-transparent.svg", import.meta.url)
    .href,
  select_icon: new URL("./selectIcon.svg", import.meta.url)
    .href,
};
