import React, { FC } from "react";
import { FaStar, FaRegStar, FaStarHalfAlt } from "react-icons/fa";
import styled from 'styled-components'

interface IRatingProps {
    rating: number;
}
const Star = styled.span`
  margin: 0.1rem;
  
  svg {
    color: #f8e825;
  }
`;
const StarContianer = styled.div`
font-size: 18px;
`

const Rating: FC<IRatingProps> = ({ rating }) => {
    return (
        <StarContianer>
            <Star>{rating >= 1 ? <FaStar /> : rating >= 0.5 ? <FaStarHalfAlt /> : <FaRegStar />}</Star>
            <Star>{rating >= 2 ? <FaStar /> : rating >= 2.5 ? <FaStarHalfAlt /> : <FaRegStar />}</Star>
            <Star>{rating >= 3 ? <FaStar /> : rating >= 3.5 ? <FaStarHalfAlt /> : <FaRegStar />}</Star>
            <Star>{rating >= 4 ? <FaStar /> : rating >= 4.5 ? <FaStarHalfAlt /> : <FaRegStar />}</Star>
            <Star>{rating >= 5 ? <FaStar /> : rating >= 5.5 ? <FaStarHalfAlt /> : <FaRegStar />}</Star>
        </StarContianer>);
};

export default Rating;
