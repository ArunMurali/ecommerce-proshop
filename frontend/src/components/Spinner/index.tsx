import React from 'react';
import styled from 'styled-components';

interface PropsType {
    width: string;
    height: string;
    color: string;
}

const Spinner: React.FC<PropsType> = (props) => {
    const Loader = styled.div`
        display: inline-block;
        position: relative;
        width: ${props => props.width};
        height: ${props => props.height};
        border: 16px solid #f3f3f3;
        border-top: 16px solid ${props => props.color};
        border-bottom: 16px solid ${props => props.color};
        border-radius: 50%;
        animation: spin 2s linear infinite;

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
        
            100% {
                transform: rotate(360deg);
            }
        }
    `;

    return (
        <Loader {...props} />
    );
}

export default Spinner;
