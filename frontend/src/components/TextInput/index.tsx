import React, { FC, ChangeEvent, useState } from "react";
import { FaRegEye, FaRegEyeSlash } from "react-icons/fa";

interface TextInputProps {
  label: string;
  type: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  value?: string;
  isPassword?: boolean;
  error?: string;
}
const TextInput: FC<TextInputProps> = ({
  label,
  type,
  onChange,
  value,
  isPassword = false,
  error,
}) => {
  const [isEye, IsEyeSlash] = useState(false);
  return (
    <div className={["form-group", error ? "error" : ""].join(" ")}>
      <input
        placeholder=" "
        value={value}
        onChange={onChange}
        type={!isEye && isPassword ? "password" : type}
        className="form-field"
      />
      <label>{label}</label>
      {isPassword &&
        (isEye ? (
          <FaRegEye className="eye-icon" onClick={() => IsEyeSlash(!isEye)} />
        ) : (
          <FaRegEyeSlash
            className="eye-icon"
            onClick={() => IsEyeSlash(!isEye)}
          />
        ))}
    </div>
  );
};

export default TextInput;
