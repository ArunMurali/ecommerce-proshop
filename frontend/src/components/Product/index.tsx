import React, { FC } from "react";
import { Card } from "react-bootstrap";
import Rating from "../Rating";
import { Link } from "react-router-dom";

interface ProductProps {
  product: {
    name: string;
    _id: string;
    image: string;
    price: number;
  };
}
const Product: FC<ProductProps> = ({ product }) => {
  console.log(product)
  return (
    <div className="card my-3 mx-3 p-3 rounded " style={{ width: "18rem;" }}>
      <Link to={`/product/${[product._id]}`}>
        <img className="card-img-top" src={product.image} />
      </Link>
      <div className="card-body">
        <Link to={`/product/${[product._id]}`}>
          <div className="card-title product-title">
            <strong>{product.name}</strong>
          </div>
        </Link>
        <div className="card-text h3">${product.price}</div>
        {/* <Rating value={4} text="" color="#ff9900" /> */}
        <Rating rating={4} />

      </div>
    </div>
  );
};

export default Product;
