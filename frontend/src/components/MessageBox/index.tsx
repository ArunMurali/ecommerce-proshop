import React, { FC, useEffect, useState } from "react";
import { FloatingOverlay } from "@floating-ui/react";

type Error = {
  email?: string;
  password?: string;
}
interface MessageBoxProps {
  message: string;
  setMessage: React.Dispatch<React.SetStateAction<string>>;
  error?: Error | null;
}


const MessageBox: FC<MessageBoxProps> = (props) => {
  const { message, setMessage, error } = props;
  const [isVisible, setIsVisible] = useState(false);


  useEffect(() => {
    if (!message || !message?.length) return;
    setIsVisible(true);
    setTimeout(() => {
      setIsVisible(false);
      setMessage('')
    }, 5000);
  }, [message]);



  return !isVisible ? null : (
    <FloatingOverlay
    >
      <div className="message-continer">
        <div className={error ? "message-error" : "message-suceesss"}>{message}</div>
      </div>
    </FloatingOverlay>
  );
};

export default MessageBox;
