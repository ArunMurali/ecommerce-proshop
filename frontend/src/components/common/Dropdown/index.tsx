import React, { useState } from "react";
import Images from "../../../assets/images";
type ArrayTypes = number | string | boolean;
import { useFloating, useInteractions, useHover, safePolygon, flip } from "@floating-ui/react";
export interface IDropdownProps {
    label: string;
    options: ArrayTypes[];
    selected?: ArrayTypes[];
    onSelect?: (selectedOption: ArrayTypes) => void;
}

export default function Dropdown(props: IDropdownProps) {
    const { selected, label, onSelect, options } = props;
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(selected);
    const { refs, floatingStyles, context, } = useFloating({
        open: isOpen,
        onOpenChange: setIsOpen,
        placement: 'bottom-start',
        middleware: [flip()],
    });
    const hover = useHover(context, { handleClose: safePolygon() });

    const { getReferenceProps, getFloatingProps } = useInteractions([hover]);
    const handleSelect = (value) => {
        setSelectedOption(value);
        onSelect && onSelect(value);
    };


    return (
        <>
            <div className="qty dropdown" ref={refs.setReference}  {...getReferenceProps}>
                {selectedOption || label}
                <img
                    className="dropdown-icon"

                    src={Images.select_icon}
                    alt="dropdown select"
                />


            </div>
            {isOpen && (<div
                ref={refs.setFloating}
                style={floatingStyles}
                className="dropdown-content"
                {...getFloatingProps}
            >
                <ul>
                    <li>{label}</li>
                    {options.map((e: ArrayTypes, i: number) => (
                        <li key={i} onClick={() => handleSelect(e)}>
                            {e}
                        </li>
                    ))}
                </ul>
            </div>)}

        </>
    );
}
