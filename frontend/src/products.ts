export default [
    {
        _id: '1',
        name: "Apple AirPods",
        images: "/images/airpods.jpg",
        description: "Wireless earbuds with superior sound quality",
        brand: "Apple",
        price: 159.99,
        countInStock: 10,
        rating: 4.7,
        numReviews: 25
    },
    {
        _id: '2',
        name: "Amazon Echo (4th Gen) - Smart speaker with Alexa",
        images: "/images/alexa.jpg",
        description: "Ask Alexa to play music, answer questions, play the news, check the weather, set alarms, control compatible smart home devices, and more",
        brand: "Amazon",
        price: 99.99,
        countInStock: 15,
        rating: 3.5,
        numReviews: 30
    },
    {
        _id: '3',
        name: "Canon EOS Rebel T7 DSLR Camera",
        images: "/images/camera.jpg",
        description: "18.0 Megapixel CMOS (APS-C) image sensor and DIGIC 4+ Image Processor for high image quality and speed",
        brand: "Canon",
        price: 499.99,
        countInStock: 5,
        rating: 3,
        numReviews: 20
    },
    {
        _id: '4',
        name: "Logitech MX Master 3 Advanced Wireless Mouse",
        images: "/images/mouse.jpg",
        description: "High-performance wireless mouse with ergonomic design and customizable buttons",
        brand: "Logitech",
        price: 99.99,
        countInStock: 20,
        rating: 4.6,
        numReviews: 35
    },
    {
        _id: '5',
        name: "Samsung Galaxy S21 5G",
        images: "/images/phone.jpg",
        description: "5G-enabled smartphone with a 6.2-inch Dynamic AMOLED 2X display and a 12MP triple camera system",
        brand: "Samsung",
        price: 799.99,
        countInStock: 8,
        rating: 4.8,
        numReviews: 40
    },
    {
        _id: '6',
        name: "PlayStation 5 Console",
        images: "/images/playstation.jpg",
        description: "Next-gen console with ultra-high-speed SSD, 4K-TV gaming, and 120fps with 3D audio",
        brand: "Sony",
        price: 499.99,
        countInStock: 3,
        rating: 4.9,
        numReviews: 50
    },
];