import React, { FC, useState } from "react";

import { useParams } from "react-router-dom";
import Rating from "../../components/Rating";
import { useGetProductDetailsQuery } from "../../slices/productApiSlice";
import Spinner from "../../components/Spinner";
import Images from "../../assets/images";
import Dropdown from "../../components/common/Dropdown";
import { addToCart } from "../../slices/cartSlice";

interface IProductDetailsProps { }

const ProductDetails: FC<IProductDetailsProps> = () => {
    const { id } = useParams();
    const { error, isLoading, data } = useGetProductDetailsQuery(id);
    return isLoading ? (
        <div className="spin-container">
            <Spinner color="#216ce7" height="120px" width="120px" />
        </div>
    ) : error ? (
        error
    ) : (
        <div className="container-fluid     product-container">
            <div className="product-image">
                <img src={data?.image} alt="" />
            </div>
            <div className="product-details">
                <h3 className="product-caption">{data?.name}</h3>
                <div className="product-price">${data?.price}</div>
                <Rating rating={data?.rating} />
                <p className="product-description">{data?.description}</p>
            </div>
            <div className="cart-details">
                <div className="price">Price: ${data?.price}</div>
                <div className="status">Status: In Stock</div>
                <Dropdown label={"Qty"} options={['item1', 'item2']} />

                <button className="add-to-cart-button">Add to Cart</button>
            </div>
        </div >
    );
};

export default ProductDetails;
