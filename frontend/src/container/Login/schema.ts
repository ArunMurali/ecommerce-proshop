const message = "^Please fill the required fields";
export const loginSchema = {
  email: {
    presence: { allowEmpty: false, message },
    email: true,
  },
  password: {
    presence: { allowEmpty: false, message },
    format: {
      pattern: /^(?=.*[a-z])(?=.*[A-Z]).{6,}$/,
      message:
        "^Password must contain at least one lower case letter, one uppercase letter and be at least 6 characters long",
    },
  },
};
