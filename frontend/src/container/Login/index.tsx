// tsrsfc
import React, { useState } from "react";
import TextInput from "../../components/TextInput";
import { Link } from "react-router-dom";
import Images from "../../assets/images";
import MessageBox from '../../components/MessageBox'
import validate from 'validate.js'
import { loginSchema } from './schema'
import { useNavigate } from 'react-router-dom'

interface ILoginProps { }
interface Error {
  email?: string;
  password?: string;
}

const Login: React.FunctionComponent<ILoginProps> = () => {
  const [isLogin, setIsLogin] = useState(true);
  const [data, setData] = useState({ email: "", password: "" });
  const [errors, setErrors] = useState<Error | null>(null);
  const [message, setMessage] = useState('');
  const navigate = useNavigate();

  const handleFieldChange = (field: string, value: string) => {
    setData({ ...data, [field]: value });
  }
  const formValidate = (): boolean => {

    const error = validate(data, loginSchema);
    setErrors(error);
    if (!error) return true;
    const res: string[] = (Object.values(error) as string[][]).flat();
    setMessage(res[0])
    // else setMessage(res.filter(e => e !== message)[0]);
    return false;
  }
  const handleSubmit = (e: any) => {
    e.preventDefault();
    const isValid = formValidate();
    if (isValid) {
      setMessage("Successfully logged")
      setTimeout(() => navigate('/product'), 2000)

    }

  }

  return (<>
    <MessageBox message={message} setMessage={setMessage} error={errors} />
    <section className="section-login">
      <div className="cirlce" />
      <div className="login-card">
        <img
          className="login-logo"
          color="red"
          src={Images.logo_white}
          alt=""
        />

        <form className="login-form">
          <h3 className="col-sm-12  mx-auto">
            {isLogin ? "Login" : "Create Account"}
          </h3>
          {!isLogin && (
            <div className="col-sm-9  mx-auto my-0">
              <TextInput type="text" label="First Name" />
            </div>
          )}
          {!isLogin && (
            <div className="col-sm-9  mx-auto my-0">
              <TextInput type="text" label="Last Name" />
            </div>
          )}
          <div className="col-sm-9  mx-auto my-0">
            <TextInput type="text" error={errors?.email} label="Email" value={data.email} onChange={(event) => handleFieldChange('email', event.target.value)} />
          </div>
          <div className="col-sm-9  mx-auto my-0">
            <TextInput isPassword type="text" label="Password" error={errors?.password} value={data.password} onChange={(event) => handleFieldChange('password', event.target.value)} />
          </div>
          {!isLogin && (
            <div className="col-sm-9  mx-auto my-0">
              <TextInput type="text" label="confirm Password" />
            </div>
          )}

          <div className="col-9  col-sm-9  mx-auto my-0">
            <button onClick={handleSubmit} className="login-btn" type="button">
              {isLogin ? "Login" : "Sign Up"}
            </button>
          </div>
        </form>
        <footer className="login-footer text-center">
          {isLogin
            ? "Need an Account? click here"
            : " Already have an account? click here"}
          &nbsp;
          <a
            onClick={() => setIsLogin((prev) => !prev)}
            href="javascript:void()"
          >
            {isLogin ? "login" : "sign up"}
          </a>
        </footer>
      </div>
    </section>
  </>
  );
};

export default Login;
