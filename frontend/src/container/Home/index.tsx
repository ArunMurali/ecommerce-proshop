import React, { memo, } from "react";
import Product from "../../components/Product";
import { useGetProductsQuery } from '../../slices/productApiSlice'
import Spinner from "../../components/Spinner";
const Home = memo(() => {

  const { data: productList, isLoading, error } = useGetProductsQuery();

  return (isLoading ? (<div className="spin-container">
    <Spinner color="#216ce7" height="120px" width="120px" />
  </div>
  )
    : error ? (<>{error}</>)
      : (<>
        <h1>Latest Products</h1>
        <div className="row ">
          {productList?.map((product) => (
            <div className="col-sm-12 col-md-6 col-lg-4 col-xl-3" key={product._id}>
              <Product product={product} />
            </div>
          ))}
        </div>
      </>)

  );
});

export default Home;
