import { createSlice } from '@reduxjs/toolkit';
const initialState = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem("cart")) : ({ cartItems: [] });
const addDecimals = (num) => {
    return (Math.round((num * 100) / 100).toFixed(2))
}
const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action) => {
            const item = action.payload;
            const existItem = state.cartItems.find(x => x._id === item._id);
            if (existItem) {
                state.cartItems = state.cartItems.map(e => e._id === item._id ? item : e);
            }
            else {
                state.cartItems = [...state.cartItems, item]
            }
            // calc items price
            state.itemsPrice = addDecimals(state.cartItems.reduce((acc, val) => (acc + val.price) * val.qty, 0))
            // calc item shipping price
            state.shippingPrice = addDecimals(state.itemPrice > 100 ? 0 : 10);
            // calc tax price
            state.taxPrice = addDecimals(Number(0.18 * state.itemsPrice).toFixed(2))

            // calc total price
            state.totalPrice = addDecimals(Number(state.itemPrice) + Number(state.shippingPrice) + Number(state.taxPrice))
            localStorage.setItem('cart', JSON.stringify(state))
        },

    }
});
export default cartSlice.reducer;
export const { addToCart } = cartSlice.actions
