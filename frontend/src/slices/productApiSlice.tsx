import { PRODUCT_URL } from '../constant'
import { apiSlice } from './apiSlice';

export const productApiSlice = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getProducts: builder.query({
            query: (arg) => ({ url: PRODUCT_URL }),
            keepUnusedDataFor: 5
        }),
        getProductDetails: builder.query({
            query: (id) => ({ url: `${PRODUCT_URL}/${id}` })
        }),
    })
});

export const { useGetProductsQuery, useGetProductDetailsQuery } = productApiSlice