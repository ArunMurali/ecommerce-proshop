import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App.tsx";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import Home from "./container/Home";
import Login from "./container/Login";
import Cart from "./container/Cart";
import "bootstrap/js/dist/collapse.js";
import "./assets/styles/index.scss";
import ProductDetails from "./container/ProductDetails/index.tsx";
import store from './store.tsx';
import { Provider } from 'react-redux'

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/">
      <Route index element={<Login />} />
      <Route element={<App />}>
        <Route path="product/" element={<Home />} />


        <Route path="product/:id" element={<ProductDetails />} />

        <Route path="cart" element={<Cart />} />
      </Route>
    </Route>
  )
);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
