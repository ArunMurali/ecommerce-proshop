import { configureStore } from '@reduxjs/toolkit'
import { apiSlice } from './slices/apiSlice'
import cartSliceReducers from './slices/cartSlice'
const store = configureStore({
    reducer: { [apiSlice.reducerPath]: apiSlice.reducer, cart: cartSliceReducers },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(apiSlice.middleware),
    devTools: true, // Enable Redux Dev Tools extension for development only
})

export default store