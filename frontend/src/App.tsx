import { useState } from "react";
import Header from "./layout/Header";
import Footer from "./layout/Footer";
import { Container } from "react-bootstrap";
import { Outlet } from "react-router-dom";

const App = () => {
  return (
    <>
      <Header />
      <main className="py-3">
        <Container className="overflow-auto" style={{ height: "80vh" }}>
          <Outlet />
        </Container>
      </main>
      <Footer />
    </>
  );
};

export default App;
