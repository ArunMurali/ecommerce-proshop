import express from 'express';
import asyncHandler from '../middleware/asyncHandler.js'
import { getProductsById, getProucts } from '../controllers/product.controller.js'
const router = express.Router();

// GET all proucts
router.route('/').get(getProucts)
// GET request for querying by id
router.route('/:id').get(getProductsById)
// POST request to create a new product
router.post('/', asyncHandler(({ body }, res) => {
    console.log("POST ROUTE HIT");
    products.push(body);
    res.json(body);
}));
export default router;