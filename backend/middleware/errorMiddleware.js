export const notFound = (req, res, next) => {
    const error = new Error(`NOt Found -${req.originalUrl}`);
    res.status(404);
    next(error)
}

export const errorHandler = (err, req, res, next) => {
    let statusCode = res.statusCode === 200 ? 500 : res.statusCode;
    let message = err.message;
    // check for mongooose bad object id
    if (err.name === 'CastError' && err.kind === 'ObjectId') {
        message = `Resource with ID ${err.value} not found.`;
        statusCode = 404;
    }
    res.status(statusCode).json({
        message: message,
        stack: process.env.NODE_ENV === "production" ? {} : err.stack,

    })



}
