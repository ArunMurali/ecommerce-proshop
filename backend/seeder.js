import mongoose from 'mongoose';
import dotenv from 'dotenv';
import colors from 'colors';
import products from './data/proudcts.js';
import users from './data/user.js';
import Order from './models/order.model.js';
import Product from './models/product.model.js';
import User from './models/user.model.js';
import connectDB from './config/db.js';
import user from './data/user.js';
dotenv.config();
connectDB();

const importData = async () => {
    try {
        await Order.deleteMany();
        await Product.deleteMany();
        await User.deleteMany();

        const createdUser = await User.insertMany(user);
        const adminUsers = createdUser[0]._id;
        const sampleProducts = products.map(product => {
            return ({ ...product, user: adminUsers })
        })
        await Product.insertMany(sampleProducts)
        console.log('Data imported...'.green.inverse);
        process.exit();
    } catch (error) {
        console.error(`${error}`.red.inverse);
        process.exit(1);
    };
};
const destroyData = async () => {
    try {
        await Order.deleteMany();
        await Product.deleteMany();
        await User.deleteMany();
        console.log('Data Destroyed...'.green.inverse);
        process.exit();
    } catch (error) {
        console.error(`${error}`.red.inverse);
        process.exit(1);
    }

}
if (process.argv[2] === '-d') {
    destroyData()
}
else {
    importData()
}