import fs from 'fs'
import path from 'path'
import { promisify } from 'util'
import { exec } from 'child_process'
import { app } from './app.js'
import { MongoClient } from 'mongodb'

// Define the path to the seed directory
const seedDirectory = 'src/seed'

// Promisify the fs.readdir function to get a list of files in the seed directory
const readdir = promisify(fs.readdir)

// Function to run all the seed files
const runSeedFiles = async () => {
    const connection = await app.get('mongodb')
    const database = new URL(connection).pathname.substring(1)
    const client = await MongoClient.connect(connection)
    const mongoClient = client.db(database)
    try {
        const files = await readdir(seedDirectory)
        let count = 0

        // Define a helper function to execute each seed file
        const executeSeedFile = async (filePath) => {
            await promisify(exec)(`node ${filePath} ${database} ${connection}`)
            console.log(`Seed file ${filePath} executed successfully.`)
        }

        // Loop through each file and execute it one by one
        for (const file of files) {
            count++
            const filePath = path.join(seedDirectory, file)
            // Execute the current seed file
            await executeSeedFile(filePath)
        }

        console.log(`${count} seed files executed successfully.`)
    } catch (error) {
        console.error('Error executing seed files:', error)
    } finally {
        // Close the database connection
        await client.close()
        console.log('Database connection closed')
    }
}

// Run the seed files
runSeedFiles()
    .then(() => {
        // Proceed to the next step or perform any other actions after running all seed files
        console.log('All seed files executed')
        process.exit()
    })
    .catch((error) => {
        console.error('Error executing seed files:', error)
        process.exit(1)
    })
