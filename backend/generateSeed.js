import fs from 'fs'
import path from 'path'

// Get the provided filename from the command line arguments
const providedFilename = process.argv[2]

// Check if the filename is provided
if (!providedFilename) {
    console.error('Please provide a filename.')
    process.exit(1)
}

const code = `import { basename, dirname } from 'node:path'
import { fileURLToPath } from 'node:url'
import { MongoClient } from 'mongodb'
import { PlatformConfig } from '../util/enum.js'

async function seedData() {
  const __filename = fileURLToPath(import.meta.url)
  const client = await MongoClient.connect(process.argv[3])
  const mongoClient = client.db(process.argv[2])
  const file_name = basename(__filename)
  const seedIsExist = await mongoClient.collection('mongodb_seeds_lock').findOne({ file_name })
  if (seedIsExist) {
    return
  } else {
    await mongoClient.collection('platformConfig').insertMany([
      {
        _id: PlatformConfig.TWILIO,
        name: 'Twilio',
        Description: 'True enable twilio and False to disable twilio',
        value: true
      }
    ])
    await mongoClient
      .collection('mongodb_seeds_lock')
      .insertOne({ file_name }, { forceServerObjectId: false })
  }
}
seedData()
  .then(() => {
    // Proceed to the next step or perform any other actions after running all seed files
    console.log('All seed files executed')
    process.exit()
  })
  .catch((error) => {
    console.error('Error executing seed files:', error)
    process.exit(1)
  })
`

const timestamp = new Date().getTime()

const rootDirectory = 'src/seed'
// Create the seed directory if it doesn't exist
fs.mkdirSync(rootDirectory, { recursive: true })

const fileName = `${timestamp}_${providedFilename}.js`
// Create the file with the provided filename in the seed directory
fs.writeFileSync(path.join(rootDirectory, fileName), code)

console.log(`Seed file "${fileName}" created successfully.`)
