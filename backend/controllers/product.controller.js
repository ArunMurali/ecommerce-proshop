import asyncHandler from '../middleware/asyncHandler.js';
import Product from '../models/product.model.js';
// // @desc    Get all products
// // @route   GET /products
// export const getProducts = asyncHandler(async (req, res) => {
//   console.log('getting all products');
//   const pageSize = +req.query.pageSize || 10;
//   const pageNum = +req.query.pageNumber || 0;
//   //const total = await Product.countDocuments();
//   //console.log(`Total number of documents: ${total}`);
//   const data = await Product.find()
//     .skip(pageNum * pageSize)
//     .limit(pageSize);
//   /*res.status(200).json({
//     success: true,
//     count: data.length,
//     pages: Math.ceil(total / pageSize),
//     page: pageNum,
//     data: data
//   });*/
//   res.json(data);
// });

// @desc  get all products
// @route GET /products
// @access Public
export const getProucts = asyncHandler(async (req, res) => {
    const product = await Product.find({})
    res.json(product);

})
// @desc Get a product
// @router  GET /products/:id
// @access Public
export const getProductsById = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id);
    if (product) {
        return res.json(product)
    } else {
        res.status(404);
        throw new Error("Resource not found");
    }

})