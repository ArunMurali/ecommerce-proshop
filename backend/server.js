import express from 'express';
import cors from 'cors'
import productRoute from './routes/product.routes.js'
import dotenv from 'dotenv'
import connectDB from './config/db.js'
import { errorHandler, notFound } from './middleware/errorMiddleware.js'
dotenv.config();
const PORT = process.env.PORT || 5000;
connectDB();
const app = express();
app.use(cors({
    origin: [process.env.BASE_URL]
}));
app.use(express.json());
app.get('/', (req, res) => {
    res.send('Hello World!');
});
app.use('/api/products', productRoute)
app.use(notFound);
app.use(errorHandler);
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));