import bcrypt from 'bcryptjs'
export default [
    {
        name: "Admin",
        email: "admin@gmail.com",
        password: bcrypt.hashSync("123456", 8),
        isAdmin: true,
    },
    {
        name: "Jane Doe",
        email: "jane@gmail.com",
        password: bcrypt.hashSync("123456", 8),
        isAdmin: false,
    },
    {
        name: "John Doe",
        email: "john@gmail.com",
        password: bcrypt.hashSync("123", 8),
        isAdmin: false,
    }
]

// Function to check if user exists by email
// function getUserByEmail(email) {
//     return users.find((user) => user.email === email);
// }

// module.exports = {
//     // Get all Users
//     getUsers: (req, res) => {
//         res.json(users);
//     },

//     //Get a single User by Email
//     getUser: (req, res) => {
//         const email = req.params.email;
//         const user = getUserByEmail(email);

//         if (!user) {
//             return res.status(400).json({ msg: "User not found!" })
//         }
//         res.json(user);
//     },
//     //Create new User
//     createUser: (req, res) => {
//         const newUser = { ...req.body };
//         let user = getUserByEmail(newUser.email);
//         if (user) {
//             return res.status(400).json({ msg: 'E-mail already in use' });
//         }
//         else {
//             console.log('New User');
//             newUser.password = bcrypt.hashSync(newUser.password, 8);
//             authors.push(newUser);
//             res.send(newUser);
//         }
//     },
//     //Update an existing User
//     updateUser: (req, res) => {
//         const email = req.params.email;
//         const userUpdates = req.body;
//         let user = getUserByEmail(email);
//         if (!user) return res.status(400).json({ msg: 'User not found' });

//         user = { ...user, ...userUpdates };
//         res.json(user);
//     },
//     //Delete a User
//     deleteUser: (req, res) => {
//         const email = req.params.email;
//         const index = users.findIndex((x) => x.email === email);

//         if (index !== -1) {
//             authors.splice(index, 1);
//         }
//         res.json({ msg: `User deleted` });
//     },
//     getToken: (req, res) => {
//         const token = signToken(req.user);
//         res.json({ token })
//     },
//     isAuthenticated: (req, res) => {
//         try {
//             const token = req.header("auth-token");
//             if (!token) throw new Error("No token provided")

//             const verified_token = verifyToken(token);
//             if (!verified_token) throw new Error("Invalid Token")
//             req.user = verified_token;
//             res.json(req.user);
//         } catch (e) {
//             res.status(401).send({ err: e.message });
//         }
//     }
// }
